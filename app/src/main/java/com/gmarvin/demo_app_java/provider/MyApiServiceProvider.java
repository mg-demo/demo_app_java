package com.gmarvin.demo_app_java.provider;

import com.gmarvin.demo_app_java.response.DataProductResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MyApiServiceProvider {

    @GET("products")
    Call<DataProductResponse> getProducts();

}
