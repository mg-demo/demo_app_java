package com.gmarvin.demo_app_java.view.product;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gmarvin.demo_app_java.R;
import com.gmarvin.demo_app_java.response.ProductResponse;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private Context mContext;
    private List<ProductResponse> productList;

    public ProductAdapter(Context mContext, List<ProductResponse> productList) {
        this.mContext = mContext;
        this.productList = productList;
    }

    public void setProductList(List<ProductResponse> productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ProductAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProductResponse pr = productList.get(position);
        holder.tvName.setText(pr.getName());
        holder.tvDescription.setText(pr.getDescription());
        holder.tvPrice.setText("NIO " + pr.getPrice());

        if (pr.getImage() != null) {
            Glide.with(mContext).load(pr.getImage()).centerCrop().into(holder.ivProduct);
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        public CardView cvItem;
        public ImageView ivProduct;
        public TextView tvName, tvDescription, tvPrice;
        public ImageButton ibShopping, ibShopping2;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cvItem = itemView.findViewById(R.id.cvItem);
            ivProduct = itemView.findViewById(R.id.ivProduct);
            tvName = itemView.findViewById(R.id.tvName);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            ibShopping = itemView.findViewById(R.id.ibShopping);
            ibShopping2 = itemView.findViewById(R.id.ibShopping2);
        }
    }

}
