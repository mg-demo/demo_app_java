package com.gmarvin.demo_app_java.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataProductResponse {

    @SerializedName("result")
    @Expose private String result;
    @SerializedName("message")
    @Expose private String message;
    @SerializedName("products")
    @Expose private List<ProductResponse> products;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProductResponse> getProducts() {
        return products;
    }

    public void setProducts(List<ProductResponse> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "DataProductResponse{" +
                "result='" + result + '\'' +
                ", message='" + message + '\'' +
                ", products=" + products +
                '}';
    }

}
