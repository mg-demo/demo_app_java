package com.gmarvin.demo_app_java.view.product;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.gmarvin.demo_app_java.R;
import com.gmarvin.demo_app_java.provider.MyApiAdapterProvider;
import com.gmarvin.demo_app_java.response.DataProductResponse;
import com.gmarvin.demo_app_java.response.ProductResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductActivity extends AppCompatActivity {

    ProductAdapter productAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        productAdapter = new ProductAdapter(this, new ArrayList<ProductResponse>());

        getData();

        RecyclerView rvProducts = findViewById(R.id.rvProducts);

        rvProducts.setLayoutManager(new GridLayoutManager(this, 2));
        rvProducts.setAdapter(productAdapter);

    }

    private void getData() {
        MyApiAdapterProvider.getApiService().getProducts().enqueue(new Callback<DataProductResponse>() {
            @Override
            public void onResponse(Call<DataProductResponse> call, Response<DataProductResponse> response) {
                if (response.isSuccessful() && response.body() !=null) {
                    productAdapter.setProductList(response.body().getProducts());
                } else {
                    Toast.makeText(getApplicationContext(), "Ah ocurrido un error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DataProductResponse> call, Throwable t) {

            }
        });
    }

}